      ******************************************************************
      * DCLGEN TABLE(FISDBT1.IVK_INV_TRK_V)                            *
      *        LIBRARY(FIST.FIS.DCLLIB(HVIVK))                         *
      *        ACTION(REPLACE)                                         *
      *        LANGUAGE(COBOL)                                         *
      *        NAMES(IVK-)                                             *
      *        QUOTE                                                   *
      *        COLSUFFIX(YES)                                          *
      * ... IS THE DCLGEN COMMAND THAT MADE THE FOLLOWING STATEMENTS   *
      ******************************************************************
           EXEC SQL DECLARE IVK_INV_TRK_V TABLE
           ( USER_CD                        CHAR(8) NOT NULL,
             LAST_ACTVY_DT                  CHAR(5) NOT NULL,
             VNDR_ACCT_NBR                  CHAR(12) NOT NULL,
             TRACKING_NBR                   CHAR(16) NOT NULL,
             TRK_FILL                       CHAR(23) NOT NULL,
             FK_IVA_DOC_NBR                 CHAR(8) NOT NULL,
             FK_IVA_ITEM_NBR                DECIMAL(4, 0) NOT NULL,
             FK_IVA_SEQ_NBR                 DECIMAL(4, 0) NOT NULL
           ) END-EXEC.
      ******************************************************************
      * COBOL DECLARATION FOR TABLE FISDBT1.IVK_INV_TRK_V              *
      ******************************************************************
       01  DCLIVK-INV-TRK-V.
      *                       USER_CD
           10 IVK-USER-CD          PIC X(8).
      *                       LAST_ACTVY_DT
           10 IVK-LAST-ACTVY-DT    PIC X(5).
      *                       VNDR_ACCT_NBR
           10 IVK-VNDR-ACCT-NBR    PIC X(12).
      *                       TRACKING_NBR
           10 IVK-TRACKING-NBR     PIC X(16).
      *                       TRK_FILL
           10 IVK-TRK-FILL         PIC X(23).
      *                       FK_IVA_DOC_NBR
           10 IVK-FK-IVA-DOC-NBR   PIC X(8).
      *                       FK_IVA_ITEM_NBR
           10 IVK-FK-IVA-ITEM-NBR  PIC S9(4)V USAGE COMP-3.
      *                       FK_IVA_SEQ_NBR
           10 IVK-FK-IVA-SEQ-NBR   PIC S9(4)V USAGE COMP-3.
      ******************************************************************
      * THE NUMBER OF COLUMNS DESCRIBED BY THIS DECLARATION IS 8       *
      ******************************************************************
