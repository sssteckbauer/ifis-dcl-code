      ******************************************************************
      * DCLGEN TABLE(WNV_WEB3270_NVGT_V)                               *
      *        LIBRARY(FISQ.FIS.DCLLIB(HVWNV))                         *
      *        ACTION(REPLACE)                                         *
      *        LANGUAGE(COBOL)                                         *
      *        NAMES(WNV-)                                             *
      *        QUOTE                                                   *
      *        COLSUFFIX(YES)                                          *
      * ... IS THE DCLGEN COMMAND THAT MADE THE FOLLOWING STATEMENTS   *
      ******************************************************************
           EXEC SQL DECLARE WNV_WEB3270_NVGT_V TABLE
           ( USER_CD                        CHAR(8) NOT NULL,
             LAST_ACTVY_DT                  CHAR(5) NOT NULL,
             SERVICE_NAME                   CHAR(24) NOT NULL,
             RECORD_KEY                     CHAR(32) NOT NULL,
             LAST_STEP_COMPLETE             CHAR(16) NOT NULL,
             REQUEST_STARTED_TS             TIMESTAMP NOT NULL,
             SOAP_INPUT_AREA                VARCHAR(500) NOT NULL
           ) END-EXEC.
      ******************************************************************
      * COBOL DECLARATION FOR TABLE WNV_WEB3270_NVGT_V                 *
      ******************************************************************
       01  WNV-WEB3270-NVGT-V.
           10 WNV-USER-CD              PIC X(8).
           10 WNV-LAST-ACTVY-DT        PIC X(5).
           10 WNV-SERVICE-NAME         PIC X(24).
           10 WNV-RECORD-KEY           PIC X(32).
           10 WNV-LAST-STEP-COMPLETE   PIC X(16).
           10 WNV-REQUEST-STARTED-TS   PIC X(26).
           10 WNV-SOAP-INPUT-AREA.
              20 WNV-SOAP-INPUT-AREA-LEN  PIC S9(4) USAGE COMP.
              20 WNV-SOAP-INPUT-AREA-TEXT PIC X(500).
      ******************************************************************
      * THE NUMBER OF COLUMNS DESCRIBED BY THIS DECLARATION IS 7       *
      ******************************************************************
