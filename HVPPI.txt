
           EXEC SQL DECLARE PPI_PRSN_PRVID_V TABLE
           (USER_CD            CHAR(8) NOT NULL WITH DEFAULT,
           LAST_ACTVY_DT       CHAR(5) NOT NULL WITH DEFAULT,
           UNVRS_CD            CHAR(2) NOT NULL WITH DEFAULT,
           PRSN_ID_DGT_ONE     CHAR(1) NOT NULL WITH DEFAULT,
           PRSN_ID_LST_NINE    CHAR(9) NOT NULL WITH DEFAULT,
           CHANGE_DT           DATE NOT NULL,
           FK_PRS_IREF_ID      DECIMAL(7) NOT NULL,
           PRS_SET_TS          TIMESTAMP NOT NULL,
           FK_PCH_IREF_ID      DECIMAL(7) NOT NULL WITH DEFAULT,
           FK_PCH_CHG_DT       DATE NOT NULL WITH DEFAULT,
           FK_PCH_SET_TS       TIMESTAMP NOT NULL WITH DEFAULT,
           PCH_SET_TS          TIMESTAMP NOT NULL WITH DEFAULT)
           END-EXEC.
       01  PPI-PPI-PRSN-PRVID.
           05  PPI-USER-CD                    PIC X(08).
           05  PPI-LAST-ACTVY-DT              PIC X(05).
           05  PPI-UNVRS-CD                   PIC X(02).
           05  PPI-PRSN-ID-DGT-ONE            PIC X(01).
           05  PPI-PRSN-ID-LST-NINE           PIC X(09).
           05  PPI-CHANGE-DT                  PIC X(10).
           05  PPI-FK-PRS-IREF-ID             PIC S9(07)         COMP-3.
           05  PPI-PRS-SET-TS                 PIC X(26).
           05  PPI-FK-PCH-IREF-ID             PIC S9(07)         COMP-3.
           05  PPI-FK-PCH-CHG-DT              PIC X(10).
           05  PPI-FK-PCH-SET-TS              PIC X(26).
           05  PPI-PCH-SET-TS                 PIC X(26).
