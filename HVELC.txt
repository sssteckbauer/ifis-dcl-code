
           EXEC SQL DECLARE ELC_ELEMCMT TABLE
           (IDD_SEQ            DECIMAL(8) NOT NULL WITH DEFAULT,
           CMT_INFO_1          CHAR(50) NOT NULL WITH DEFAULT,
           CMT_INFO_2          CHAR(50) NOT NULL WITH DEFAULT,
           CMT_ID              DECIMAL(8) NOT NULL WITH DEFAULT,
           FK_ELC_INQ_NM       CHAR(32) NOT NULL,
           FK_ELC_PKEY_TS      TIMESTAMP NOT NULL,
           ELC_TS              TIMESTAMP NOT NULL)
           END-EXEC.
       01  ELC-ELC-ELEMCMT.
           05  ELC-IDD-SEQ                    PIC S9(8)          COMP-3.
           05  ELC-CMT-INFO-1                 PIC X(50).
           05  ELC-CMT-INFO-2                 PIC X(50).
           05  ELC-CMT-ID                     PIC S9(8)          COMP-3.
           05  ELC-FK-ELC-INQ-NM              PIC X(32).
           05  ELC-FK-ELC-PKEY-TS             PIC X(26).
           05  ELC-ELC-TS                     PIC X(26).
