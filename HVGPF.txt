      ******************************************************************
      * DCLGEN TABLE(GPF_FUND_GROUP_V)                                 *
      *        LIBRARY(FISP.FIS.DCLLIB(HVGPF))                         *
      *        LANGUAGE(COBOL)                                         *
      *        NAMES(GPF-)                                             *
      *        STRUCTURE(GPF-FUND-GROUP)                               *
      *        QUOTE                                                   *
      *        COLSUFFIX(YES)                                          *
      * ... IS THE DCLGEN COMMAND THAT MADE THE FOLLOWING STATEMENTS   *
      ******************************************************************
           EXEC SQL DECLARE GPF_FUND_GROUP_V TABLE
           ( USER_CD                        CHAR(8) NOT NULL,
             LAST_ACTVY_DT                  CHAR(5) NOT NULL,
             FUND_GRP_CD                    CHAR(6) NOT NULL,
             FUND_GRP_DESC                  CHAR(60) NOT NULL,
             IFIS_FUND_NUMBER               CHAR(6) NOT NULL,
             FROM_FUND_NUMBER               CHAR(5) NOT NULL,
             TO_FUND_NUMBER                 CHAR(5) NOT NULL,
             IFIS_FUND_LEVEL                CHAR(1) NOT NULL,
             BUDT_FUND_CD                   CHAR(1) NOT NULL,
             FUND_RESTR_CD                  CHAR(1) NOT NULL,
             MOP_IND                        CHAR(1) NOT NULL,
             EMDWNT_RSTR_CD                 CHAR(1) NOT NULL,
             SPNSR_CD                       CHAR(1) NOT NULL,
             SPNSR_CAT_CD                   CHAR(1) NOT NULL,
             TYP_AWD_CD                     CHAR(1) NOT NULL,
             ON_OFF_CAM_CD                  CHAR(1) NOT NULL,
             FED_FL_THRU_CD                 CHAR(1) NOT NULL,
             FUND_GRP_TYPE                  CHAR(1) NOT NULL
           ) END-EXEC.
      ******************************************************************
      * COBOL DECLARATION FOR TABLE GPF_FUND_GROUP_V                   *
      ******************************************************************
       01  GPF-FUND-GROUP.
      *                       USER_CD
           10 GPF-USER-CD          PIC X(8).
      *                       LAST_ACTVY_DT
           10 GPF-LAST-ACTVY-DT    PIC X(5).
      *                       FUND_GRP_CD
           10 GPF-FUND-GRP-CD      PIC X(6).
      *                       FUND_GRP_DESC
           10 GPF-FUND-GRP-DESC    PIC X(60).
      *                       IFIS_FUND_NUMBER
           10 GPF-IFIS-FUND-NUMBER
              PIC X(6).
      *                       FROM_FUND_NUMBER
           10 GPF-FROM-FUND-NUMBER
              PIC X(5).
      *                       TO_FUND_NUMBER
           10 GPF-TO-FUND-NUMBER   PIC X(5).
      *                       IFIS_FUND_LEVEL
           10 GPF-IFIS-FUND-LEVEL  PIC X(1).
      *                       BUDT_FUND_CD
           10 GPF-BUDT-FUND-CD     PIC X(1).
      *                       FUND_RESTR_CD
           10 GPF-FUND-RESTR-CD    PIC X(1).
      *                       MOP_IND
           10 GPF-MOP-IND          PIC X(1).
      *                       EMDWNT_RSTR_CD
           10 GPF-EMDWNT-RSTR-CD   PIC X(1).
      *                       SPNSR_CD
           10 GPF-SPNSR-CD         PIC X(1).
      *                       SPNSR_CAT_CD
           10 GPF-SPNSR-CAT-CD     PIC X(1).
      *                       TYP_AWD_CD
           10 GPF-TYP-AWD-CD       PIC X(1).
      *                       ON_OFF_CAM_CD
           10 GPF-ON-OFF-CAM-CD    PIC X(1).
      *                       FED_FL_THRU_CD
           10 GPF-FED-FL-THRU-CD   PIC X(1).
      *                       FUND_GRP_TYPE
           10 GPF-FUND-GRP-TYPE    PIC X(1).
      ******************************************************************
      * THE NUMBER OF COLUMNS DESCRIBED BY THIS DECLARATION IS 18      *
      ******************************************************************
