
           EXEC SQL DECLARE UCD_USER_CDS_V TABLE
           (USER_CD            CHAR(8) NOT NULL WITH DEFAULT,
           LAST_ACTVY_DT       CHAR(5) NOT NULL WITH DEFAULT,
           UNVRS_CD            CHAR(2) NOT NULL WITH DEFAULT,
           DATA_CD             CHAR(6) NOT NULL,
           ACTVY_DT            DATE NOT NULL WITH DEFAULT,
           SHORT_DESC          CHAR(10) NOT NULL WITH DEFAULT,
           STD_LONG_DESC       CHAR(35) NOT NULL WITH DEFAULT,
           RQRD_DATA_VAL_FLAG  CHAR(1) NOT NULL WITH DEFAULT,
           VLDTN_IND           CHAR(1) NOT NULL WITH DEFAULT)
           END-EXEC.
       01  UCD-UCD-USER-CDS.
           05  UCD-USER-CD                    PIC X(08).
           05  UCD-LAST-ACTVY-DT              PIC X(05).
           05  UCD-UNVRS-CD                   PIC X(02).
           05  UCD-DATA-CD                    PIC X(06).
           05  UCD-ACTVY-DT                   PIC X(10).
           05  UCD-SHORT-DESC                 PIC X(10).
           05  UCD-STD-LONG-DESC              PIC X(35).
           05  UCD-RQRD-DATA-VAL-FLAG         PIC X(01).
           05  UCD-VLDTN-IND                  PIC X(01).
