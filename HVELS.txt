
FCSAH *    EXEC SQL DECLARE ELS_ELEMSYN TABLE
FCSAH      EXEC SQL DECLARE ELS_ELEMSYN_V TABLE
           (ES_NAME            CHAR(32) NOT NULL,
           ES_GROUP            CHAR(32) NOT NULL WITH DEFAULT,
           BUILDER             CHAR(1) NOT NULL WITH DEFAULT,
           SYN_TYPE            CHAR(1) NOT NULL WITH DEFAULT,
           PKEY_TS             TIMESTAMP NOT NULL,
           FK_ELS_INQ_NM       CHAR(32) NOT NULL WITH DEFAULT,
           FK_ELS_PKEY_TS      TIMESTAMP NOT NULL WITH DEFAULT,
           ELS_TS              TIMESTAMP NOT NULL WITH DEFAULT)
           END-EXEC.
       01  ELS-ELS-ELEMSYN.
           05  ELS-ES-NAME                    PIC X(32).
           05  ELS-ES-GROUP                   PIC X(32).
           05  ELS-BUILDER                    PIC X(01).
           05  ELS-SYN-TYPE                   PIC X(01).
           05  ELS-PKEY-TS                    PIC X(26).
           05  ELS-FK-ELS-INQ-NM              PIC X(32).
           05  ELS-FK-ELS-PKEY-TS             PIC X(26).
           05  ELS-ELS-TS                     PIC X(26).
