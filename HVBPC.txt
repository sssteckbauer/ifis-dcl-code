
           EXEC SQL DECLARE BPC_BUY_POCLAS_V TABLE
           (USER_CD            CHAR(8) NOT NULL WITH DEFAULT,
           LAST_ACTVY_DT       CHAR(5) NOT NULL WITH DEFAULT,
           FK_POC_PO_CLS_CD    CHAR(1) NOT NULL,
           FK_BYR_BUYER_CD     CHAR(4) NOT NULL WITH DEFAULT,
           POC_SET_TS          TIMESTAMP NOT NULL)
           END-EXEC.
       01  BPC-BPC-BUY-POCLAS.
           05  BPC-USER-CD                    PIC X(08).
           05  BPC-LAST-ACTVY-DT              PIC X(05).
           05  BPC-FK-POC-PO-CLS-CD           PIC X(01).
           05  BPC-FK-BYR-BUYER-CD            PIC X(04).
           05  BPC-POC-SET-TS                 PIC X(26).
