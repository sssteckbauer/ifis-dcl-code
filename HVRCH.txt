
           EXEC SQL DECLARE RCH_RCVNG_HDR_V TABLE
           (USER_CD            CHAR(8) NOT NULL WITH DEFAULT,
           LAST_ACTVY_DT       CHAR(5) NOT NULL WITH DEFAULT,
           PKNG_SLIP           CHAR(15) NOT NULL WITH DEFAULT,
           BILL_OF_LADING      CHAR(10) NOT NULL WITH DEFAULT,
           RCVG_DT             DATE NOT NULL,
           FRGHT_CHRG          DECIMAL(12,2) NOT NULL WITH DEFAULT,
           NBR_PCS             DECIMAL(4) NOT NULL WITH DEFAULT,
           SHIP_WT             CHAR(8) NOT NULL WITH DEFAULT,
           CARR_IREF_ID        DECIMAL(7) NOT NULL WITH DEFAULT,
           RCVG_MTHD_CD        CHAR(4) NOT NULL WITH DEFAULT,
           PYMT_CD             CHAR(2) NOT NULL WITH DEFAULT,
           CARR_ENTPSN_IND     CHAR(1) NOT NULL WITH DEFAULT,
           DOC_REF_NBR         CHAR(10) NOT NULL WITH DEFAULT,
           UNVRS_CD            CHAR(2) NOT NULL WITH DEFAULT,
           POH_SETF            CHAR(1) NOT NULL WITH DEFAULT,
           FK_POH_PO_NBR       CHAR(8) NOT NULL,
           POH_SET_TS          TIMESTAMP NOT NULL,
           IDX_RCVNG_TS        TIMESTAMP NOT NULL WITH DEFAULT)
           END-EXEC.
       01  RCH-RCH-RCVNG-HDR.
           05  RCH-USER-CD                    PIC X(08).
           05  RCH-LAST-ACTVY-DT              PIC X(05).
           05  RCH-PKNG-SLIP                  PIC X(15).
           05  RCH-BILL-OF-LADING             PIC X(10).
           05  RCH-RCVG-DT                    PIC X(10).
           05  RCH-FRGHT-CHRG                 PIC S9(10)V99      COMP-3.
           05  RCH-NBR-PCS                    PIC S9(04)         COMP-3.
           05  RCH-SHIP-WT                    PIC X(08).
           05  RCH-CARR-IREF-ID               PIC S9(07)         COMP-3.
           05  RCH-RCVG-MTHD-CD               PIC X(04).
           05  RCH-PYMT-CD                    PIC X(02).
           05  RCH-CARR-ENTPSN-IND            PIC X(01).
           05  RCH-DOC-REF-NBR                PIC X(10).
           05  RCH-UNVRS-CD                   PIC X(02).
           05  RCH-POH-SETF                   PIC X(1).
           05  RCH-FK-POH-PO-NBR              PIC X(08).
           05  RCH-POH-SET-TS                 PIC X(26).
           05  RCH-IDX-RCVNG-TS               PIC X(26).
