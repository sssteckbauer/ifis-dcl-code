
           EXEC SQL DECLARE RED_RULE_EDITS_V TABLE
           (USER_CD            CHAR(8) NOT NULL WITH DEFAULT,
           LAST_ACTVY_DT       CHAR(5) NOT NULL WITH DEFAULT,
           SEQ_NBR             DECIMAL(4) NOT NULL,
           EDIT_CD             CHAR(4) NOT NULL WITH DEFAULT,
           ERROR_SVRTY_IND     CHAR(1) NOT NULL WITH DEFAULT,
           CNTNU_ERROR_IND     CHAR(1) NOT NULL WITH DEFAULT,
           ERROR_MSG           CHAR(39) NOT NULL WITH DEFAULT,
           OPER                CHAR(3) NOT NULL WITH DEFAULT,
           LTRL_FIELD_1        CHAR(30) NOT NULL WITH DEFAULT,
           LTRL_FIELD_2        CHAR(30) NOT NULL WITH DEFAULT,
           ELMNT_NAME          CHAR(30) NOT NULL WITH DEFAULT,
           FK_RLE_RULE_CLS_CD  CHAR(4) NOT NULL,
           FK_RLE_START_DT     DATE NOT NULL,
           FK_RLE_TIME_STMP    TIME NOT NULL)
           END-EXEC.
       01  RED-RED-RULE-EDITS.
           05  RED-USER-CD                    PIC X(08).
           05  RED-LAST-ACTVY-DT              PIC X(05).
           05  RED-SEQ-NBR                    PIC S9(04)         COMP-3.
           05  RED-EDIT-CD                    PIC X(04).
           05  RED-ERROR-SVRTY-IND            PIC X(01).
           05  RED-CNTNU-ERROR-IND            PIC X(01).
           05  RED-ERROR-MSG                  PIC X(39).
           05  RED-OPER                       PIC X(03).
           05  RED-LTRL-FIELD-1               PIC X(30).
           05  RED-LTRL-FIELD-2               PIC X(30).
           05  RED-ELMNT-NAME                 PIC X(30).
           05  RED-FK-RLE-RULE-CLS-CD         PIC X(04).
           05  RED-FK-RLE-START-DT            PIC X(10).
           05  RED-FK-RLE-TIME-STMP           PIC X(8).
