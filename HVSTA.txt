
           EXEC SQL DECLARE STA_STATE_V TABLE
           (USER_CD            CHAR(8) NOT NULL WITH DEFAULT,
           LAST_ACTVY_DT       CHAR(5) NOT NULL WITH DEFAULT,
           FK_UNV_UNVRS_CD     CHAR(2) NOT NULL WITH DEFAULT,
           CNTRY_CD            CHAR(2) NOT NULL,
           STATE_CD            CHAR(2) NOT NULL,
           FULL_NAME           CHAR(35) NOT NULL WITH DEFAULT,
           RCRD_USED_FLAG      CHAR(1) NOT NULL WITH DEFAULT,
           FK_CTY_CNTRY_CD     CHAR(2) NOT NULL WITH DEFAULT)
           END-EXEC.
       01  STA-STA-STATE.
           05  STA-USER-CD                    PIC X(08).
           05  STA-LAST-ACTVY-DT              PIC X(05).
           05  STA-FK-UNV-UNVRS-CD            PIC X(02).
           05  STA-CNTRY-CD                   PIC X(02).
           05  STA-STATE-CD                   PIC X(02).
           05  STA-FULL-NAME                  PIC X(35).
           05  STA-RCRD-USED-FLAG             PIC X(01).
           05  STA-FK-CTY-CNTRY-CD            PIC X(02).
