
           EXEC SQL DECLARE TBH_TABL_HDR_V TABLE
           (USER_CD            CHAR(8) NOT NULL WITH DEFAULT,
           LAST_ACTVY_DT       CHAR(5) NOT NULL WITH DEFAULT,
           UNVRS_CD            CHAR(2) NOT NULL WITH DEFAULT,
           APLCN_CD            CHAR(8) NOT NULL,
           TABLE_CD            CHAR(8) NOT NULL,
           FNCTN_DESC          CHAR(28) NOT NULL WITH DEFAULT,
           TBL_DTL_CD_LGTH     DECIMAL(1) NOT NULL WITH DEFAULT,
           DTL_CD_CLMN_LBL_1   CHAR(10) NOT NULL WITH DEFAULT,
           DTL_CD_CLMN_LBL_2   CHAR(10) NOT NULL WITH DEFAULT,
           FLAG_1_LABEL_1      CHAR(5) NOT NULL WITH DEFAULT,
           FLAG_1_LABEL_2      CHAR(5) NOT NULL WITH DEFAULT,
           FLAG_1_VALUE_1      CHAR(1) NOT NULL WITH DEFAULT,
           FLAG_1_VALUE_2      CHAR(1) NOT NULL WITH DEFAULT,
           FLAG_1_VALUE_3      CHAR(1) NOT NULL WITH DEFAULT,
           FLAG_1_VALUE_4      CHAR(1) NOT NULL WITH DEFAULT,
           FLAG_1_VALUE_5      CHAR(1) NOT NULL WITH DEFAULT,
           FLAG_1_DFLT_VALUE   CHAR(1) NOT NULL WITH DEFAULT,
           FLAG_2_LABEL_1      CHAR(5) NOT NULL WITH DEFAULT,
           FLAG_2_LABEL_2      CHAR(5) NOT NULL WITH DEFAULT,
           FLAG_2_VALUE_1      CHAR(1) NOT NULL WITH DEFAULT,
           FLAG_2_VALUE_2      CHAR(1) NOT NULL WITH DEFAULT,
           FLAG_2_VALUE_3      CHAR(1) NOT NULL WITH DEFAULT,
           FLAG_2_VALUE_4      CHAR(1) NOT NULL WITH DEFAULT,
           FLAG_2_VALUE_5      CHAR(1) NOT NULL WITH DEFAULT,
           FLAG_2_DFLT_VALUE   CHAR(1) NOT NULL WITH DEFAULT)
           END-EXEC.
       01  TBH-TBH-TABL-HDR.
           05  TBH-USER-CD                    PIC X(08).
           05  TBH-LAST-ACTVY-DT              PIC X(05).
           05  TBH-UNVRS-CD                   PIC X(02).
           05  TBH-APLCN-CD                   PIC X(08).
           05  TBH-TABLE-CD                   PIC X(08).
           05  TBH-FNCTN-DESC                 PIC X(28).
           05  TBH-TBL-DTL-CD-LGTH            PIC S9(01)         COMP-3.
           05  TBH-DTL-CD-CLMN-LBL-1          PIC X(10).
           05  TBH-DTL-CD-CLMN-LBL-2          PIC X(10).
           05  TBH-FLAG-1-LABEL-1             PIC X(05).
           05  TBH-FLAG-1-LABEL-2             PIC X(05).
           05  TBH-FLAG-1-VALUE-1             PIC X(01).
           05  TBH-FLAG-1-VALUE-2             PIC X(01).
           05  TBH-FLAG-1-VALUE-3             PIC X(01).
           05  TBH-FLAG-1-VALUE-4             PIC X(01).
           05  TBH-FLAG-1-VALUE-5             PIC X(01).
           05  TBH-FLAG-1-DFLT-VALUE          PIC X(01).
           05  TBH-FLAG-2-LABEL-1             PIC X(05).
           05  TBH-FLAG-2-LABEL-2             PIC X(05).
           05  TBH-FLAG-2-VALUE-1             PIC X(01).
           05  TBH-FLAG-2-VALUE-2             PIC X(01).
           05  TBH-FLAG-2-VALUE-3             PIC X(01).
           05  TBH-FLAG-2-VALUE-4             PIC X(01).
           05  TBH-FLAG-2-VALUE-5             PIC X(01).
           05  TBH-FLAG-2-DFLT-VALUE          PIC X(01).
