
           EXEC SQL DECLARE MSL_MSG_LINE_V TABLE
           (CMT_ID             DECIMAL(8) NOT NULL WITH DEFAULT,
           MDRDEST1            CHAR(8) NOT NULL WITH DEFAULT,
           MDRDEST2            CHAR(8) NOT NULL WITH DEFAULT,
           MDROSDSC            CHAR(2) NOT NULL WITH DEFAULT,
           MDROSRTC            CHAR(2) NOT NULL WITH DEFAULT,
           MDRDSTID            CHAR(8) NOT NULL WITH DEFAULT,
           MDRSEVCD            CHAR(1) NOT NULL WITH DEFAULT,
           MDRTEXTL            DECIMAL(4) NOT NULL WITH DEFAULT,
           MSG_1               CHAR(66) NOT NULL WITH DEFAULT,
           MSG_2               CHAR(66) NOT NULL WITH DEFAULT,
           FK_MSG_MSG_KEY      CHAR(8) NOT NULL,
           MSG_TS              TIMESTAMP NOT NULL)
           END-EXEC.
       01  MSL-MSL-MSG-LINE.
           05  MSL-CMT-ID                     PIC S9(8)          COMP-3.
           05  MSL-MDRDEST1                   PIC X(08).
           05  MSL-MDRDEST2                   PIC X(08).
           05  MSL-MDROSDSC                   PIC X(02).
           05  MSL-MDROSRTC                   PIC X(02).
           05  MSL-MDRDSTID                   PIC X(08).
           05  MSL-MDRSEVCD                   PIC X(01).
           05  MSL-MDRTEXTL                   PIC S9(4)          COMP-3.
           05  MSL-MSG-1                      PIC X(66).
           05  MSL-MSG-2                      PIC X(66).
           05  MSL-FK-MSG-MSG-KEY             PIC X(08).
           05  MSL-MSG-TS                     PIC X(26).
