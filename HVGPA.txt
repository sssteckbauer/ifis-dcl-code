      ******************************************************************
      * DCLGEN TABLE(GPA_ACCT_GROUP_V)                                 *
      *        LIBRARY(FISP.FIS.DCLLIB(HVGPA))                         *
      *        LANGUAGE(COBOL)                                         *
      *        NAMES(GPA-)                                             *
      *        STRUCTURE(GPA-ACCT-GROUP)                               *
      *        QUOTE                                                   *
      *        COLSUFFIX(YES)                                          *
      * ... IS THE DCLGEN COMMAND THAT MADE THE FOLLOWING STATEMENTS   *
      ******************************************************************
           EXEC SQL DECLARE GPA_ACCT_GROUP_V TABLE
           ( USER_CD                        CHAR(8) NOT NULL,
             LAST_ACTVY_DT                  CHAR(5) NOT NULL,
             ACCT_GROUP_CD                  CHAR(6) NOT NULL,
             ACCT_GROUP_DESC                CHAR(60) NOT NULL,
             IFIS_ACCT_NUMBER               CHAR(6) NOT NULL
           ) END-EXEC.
      ******************************************************************
      * COBOL DECLARATION FOR TABLE GPA_ACCT_GROUP_V                   *
      ******************************************************************
       01  GPA-ACCT-GROUP.
      *                       USER_CD
           10 GPA-USER-CD          PIC X(8).
      *                       LAST_ACTVY_DT
           10 GPA-LAST-ACTVY-DT    PIC X(5).
      *                       ACCT_GROUP_CD
           10 GPA-ACCT-GROUP-CD    PIC X(6).
      *                       ACCT_GROUP_DESC
           10 GPA-ACCT-GROUP-DESC  PIC X(60).
      *                       IFIS_ACCT_NUMBER
           10 GPA-IFIS-ACCT-NUMBER
              PIC X(6).
      ******************************************************************
      * THE NUMBER OF COLUMNS DESCRIBED BY THIS DECLARATION IS 5       *
      ******************************************************************
