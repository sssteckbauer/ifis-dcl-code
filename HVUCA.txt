      ******************************************************************
      * DCLGEN TABLE(UCA_PERM_ACCOUNT_V)                               *
      *        LIBRARY(FISP.FIS.DCLLIB(HVUCA))                         *
      *        LANGUAGE(COBOL)                                         *
      *        NAMES(UCA-)                                             *
      *        STRUCTURE(UCA-ACCT-PERM)                                *
      *        QUOTE                                                   *
      *        COLSUFFIX(YES)                                          *
      * ... IS THE DCLGEN COMMAND THAT MADE THE FOLLOWING STATEMENTS   *
      ******************************************************************
           EXEC SQL DECLARE UCA_PERM_ACCOUNT_V TABLE
           ( USER_CD                        CHAR(8) NOT NULL,
             LAST_ACTVY_DT                  CHAR(5) NOT NULL,
             PRM_MJOR_LCTN                  CHAR(2) NOT NULL,
             PRM_UNIV_W_IND                 CHAR(1) NOT NULL,
             PRM_ORG_R_LCTN                 CHAR(1) NOT NULL,
             ACCT_NUMBER                    CHAR(6) NOT NULL,
             ACCOUNT_NAME                   CHAR(35) NOT NULL,
             ARC                            CHAR(6) NOT NULL,
             UAS                            CHAR(6) NOT NULL,
             UAS_ACAD_CODE                  CHAR(3) NOT NULL,
             NSF_CODE                       CHAR(3) NOT NULL,
             MARINE_SCI_IND                 CHAR(1) NOT NULL,
             SAU                            CHAR(1) NOT NULL,
             PROGRAM_CODE                   CHAR(6) NOT NULL,
             ACCT_GRP_CODE                  CHAR(6) NOT NULL,
             ORG_UN_LVL1                    CHAR(6) NOT NULL,
             ORG_UN_LVL2                    CHAR(6) NOT NULL,
             ORG_UN_LVL3                    CHAR(6) NOT NULL,
             ORG_UN_NA_LVL1                 CHAR(30) NOT NULL,
             ORG_UN_NA_LVL2                 CHAR(30) NOT NULL,
             ORG_UN_NA_LVL3                 CHAR(30) NOT NULL,
             IFIS_NBR                       CHAR(6) NOT NULL,
             IFIS_INTRNL_CD                 CHAR(4) NOT NULL,
             ACTVY_DT                       DATE NOT NULL,
             STATUS                         CHAR(1) NOT NULL,
             FSCL_YR                        CHAR(2) NOT NULL
           ) END-EXEC.
      ******************************************************************
      * COBOL DECLARATION FOR TABLE UCA_PERM_ACCOUNT_V                 *
      ******************************************************************
       01  UCA-ACCT-PERM.
      *                       USER_CD
           10 UCA-USER-CD          PIC X(8).
      *                       LAST_ACTVY_DT
           10 UCA-LAST-ACTVY-DT    PIC X(5).
      *                       PRM_MJOR_LCTN
           10 UCA-PRM-MJOR-LCTN    PIC X(2).
      *                       PRM_UNIV_W_IND
           10 UCA-PRM-UNIV-W-IND   PIC X(1).
      *                       PRM_ORG_R_LCTN
           10 UCA-PRM-ORG-R-LCTN   PIC X(1).
      *                       ACCT_NUMBER
           10 UCA-ACCT-NUMBER      PIC X(6).
      *                       ACCOUNT_NAME
           10 UCA-ACCOUNT-NAME     PIC X(35).
      *                       ARC
           10 UCA-ARC              PIC X(6).
      *                       UAS
           10 UCA-UAS              PIC X(6).
      *                       UAS_ACAD_CODE
           10 UCA-UAS-ACAD-CODE    PIC X(3).
      *                       NSF_CODE
           10 UCA-NSF-CODE         PIC X(3).
      *                       MARINE_SCI_IND
           10 UCA-MARINE-SCI-IND   PIC X(1).
      *                       SAU
           10 UCA-SAU              PIC X(1).
      *                       PROGRAM_CODE
           10 UCA-PROGRAM-CODE     PIC X(6).
      *                       ACCT_GRP_CODE
           10 UCA-ACCT-GRP-CODE    PIC X(6).
      *                       ORG_UN_LVL1
           10 UCA-ORG-UN-LVL1      PIC X(6).
      *                       ORG_UN_LVL2
           10 UCA-ORG-UN-LVL2      PIC X(6).
      *                       ORG_UN_LVL3
           10 UCA-ORG-UN-LVL3      PIC X(6).
      *                       ORG_UN_NA_LVL1
           10 UCA-ORG-UN-NA-LVL1   PIC X(30).
      *                       ORG_UN_NA_LVL2
           10 UCA-ORG-UN-NA-LVL2   PIC X(30).
      *                       ORG_UN_NA_LVL3
           10 UCA-ORG-UN-NA-LVL3   PIC X(30).
      *                       IFIS_NBR
           10 UCA-IFIS-NBR         PIC X(6).
      *                       IFIS_INTRNL_CD
           10 UCA-IFIS-INTRNL-CD   PIC X(4).
      *                       ACTVY_DT
           10 UCA-ACTVY-DT         PIC X(10).
      *                       STATUS
           10 UCA-STATUS           PIC X(1).
      *                       FSCL_YR
           10 UCA-FSCL-YR          PIC X(2).
      ******************************************************************
      * THE NUMBER OF COLUMNS DESCRIBED BY THIS DECLARATION IS 26      *
      ******************************************************************
