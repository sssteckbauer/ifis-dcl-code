
           EXEC SQL DECLARE LTN_LCTN_V TABLE
           (USER_CD            CHAR(8) NOT NULL WITH DEFAULT,
           LAST_ACTVY_DT       CHAR(5) NOT NULL WITH DEFAULT,
           UNVRS_CD            CHAR(2) NOT NULL WITH DEFAULT,
           COA_CD              CHAR(1) NOT NULL WITH DEFAULT,
           LCTN_CD             CHAR(6) NOT NULL,
           ACTVY_DT            DATE NOT NULL WITH DEFAULT)
           END-EXEC.
       01  LTN-LTN-LCTN.
           05  LTN-USER-CD                    PIC X(08).
           05  LTN-LAST-ACTVY-DT              PIC X(05).
           05  LTN-UNVRS-CD                   PIC X(02).
           05  LTN-COA-CD                     PIC X(01).
           05  LTN-LCTN-CD                    PIC X(06).
           05  LTN-ACTVY-DT                   PIC X(10).
