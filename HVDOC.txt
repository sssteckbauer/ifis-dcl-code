
           EXEC SQL DECLARE DOC_DOCUMENT_V TABLE
           (USER_CD            CHAR(8) NOT NULL WITH DEFAULT,
           LAST_ACTVY_DT       CHAR(5) NOT NULL WITH DEFAULT,
           UNVRS_CD            CHAR(2) NOT NULL WITH DEFAULT,
           SEQ_NBR             DECIMAL(4) NOT NULL,
           DOC_TYP             CHAR(3) NOT NULL WITH DEFAULT,
           DOC_DESC            CHAR(35) NOT NULL WITH DEFAULT,
           START_DT            DATE NOT NULL WITH DEFAULT,
           END_DT              DATE NOT NULL WITH DEFAULT,
           ACTVY_DT            DATE NOT NULL WITH DEFAULT)
           END-EXEC.
       01  DOC-DOC-DOCUMENT.
           05  DOC-USER-CD                    PIC X(08).
           05  DOC-LAST-ACTVY-DT              PIC X(05).
           05  DOC-UNVRS-CD                   PIC X(02).
           05  DOC-SEQ-NBR                    PIC S9(04)         COMP-3.
           05  DOC-DOC-TYP                    PIC X(03).
           05  DOC-DOC-DESC                   PIC X(35).
           05  DOC-START-DT                   PIC X(10).
           05  DOC-END-DT                     PIC X(10).
           05  DOC-ACTVY-DT                   PIC X(10).
