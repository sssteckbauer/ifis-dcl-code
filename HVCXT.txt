
           EXEC SQL DECLARE CXT_COMM_TTYP_V TABLE
           (USER_CD            CHAR(8) NOT NULL WITH DEFAULT,
           LAST_ACTVY_DT       CHAR(5) NOT NULL WITH DEFAULT,
           UNVRS_CD            CHAR(2) NOT NULL WITH DEFAULT,
           TEXT_CD             CHAR(4) NOT NULL,
           SHORT_DESC          CHAR(10) NOT NULL WITH DEFAULT,
           LONG_DESC           CHAR(30) NOT NULL WITH DEFAULT,
           LINE_LGTH           DECIMAL(3) NOT NULL WITH DEFAULT,
           LINES_PER_PAGE      DECIMAL(3) NOT NULL WITH DEFAULT,
           TOP_MARGIN          DECIMAL(2) NOT NULL WITH DEFAULT,
           LABEL_ID            CHAR(1) NOT NULL WITH DEFAULT,
           VERIFY_FLAG         CHAR(1) NOT NULL WITH DEFAULT,
           FK_CPS_COMM_VIEW    CHAR(8) NOT NULL,
           FK_CPS_COMM_TYP     CHAR(4) NOT NULL,
           FK_CPS_COMM_PLN_CD  CHAR(4) NOT NULL,
           FK_CPS_TXT_CD_SEQ   DECIMAL(3) NOT NULL)
           END-EXEC.
       01  CXT-CXT-COMM-TTYP.
           05  CXT-USER-CD                    PIC X(08).
           05  CXT-LAST-ACTVY-DT              PIC X(05).
           05  CXT-UNVRS-CD                   PIC X(02).
           05  CXT-TEXT-CD                    PIC X(04).
           05  CXT-SHORT-DESC                 PIC X(10).
           05  CXT-LONG-DESC                  PIC X(30).
           05  CXT-LINE-LGTH                  PIC S9(03)         COMP-3.
           05  CXT-LINES-PER-PAGE             PIC S9(03)         COMP-3.
           05  CXT-TOP-MARGIN                 PIC S9(02)         COMP-3.
           05  CXT-LABEL-ID                   PIC X(01).
           05  CXT-VERIFY-FLAG                PIC X(01).
           05  CXT-FK-CPS-COMM-VIEW           PIC X(08).
           05  CXT-FK-CPS-COMM-TYP            PIC X(04).
           05  CXT-FK-CPS-COMM-PLN-CD         PIC X(04).
           05  CXT-FK-CPS-TXT-CD-SEQ          PIC S9(03)         COMP-3.
