
           EXEC SQL DECLARE LPI_LSTASGNPID_V TABLE
           (USER_CD            CHAR(8) NOT NULL WITH DEFAULT,
           LAST_ACTVY_DT       CHAR(5) NOT NULL WITH DEFAULT,
           UNVRS_CD            CHAR(2) NOT NULL,
           ALPHA_PREFIX        CHAR(1) NOT NULL WITH DEFAULT,
           LST_ASSGND_PID      DECIMAL(7) NOT NULL WITH DEFAULT)
           END-EXEC.
       01  LPI-LPI-LSTASGNPID.
           05  LPI-USER-CD                    PIC X(08).
           05  LPI-LAST-ACTVY-DT              PIC X(05).
           05  LPI-UNVRS-CD                   PIC X(02).
           05  LPI-ALPHA-PREFIX               PIC X(01).
           05  LPI-LST-ASSGND-PID             PIC S9(07)         COMP-3.
