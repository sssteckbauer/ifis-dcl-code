
           EXEC SQL DECLARE RID_RQST_ID_V TABLE
           (USER_CD            CHAR(8) NOT NULL WITH DEFAULT,
           LAST_ACTVY_DT       CHAR(5) NOT NULL WITH DEFAULT,
           UNVRS_CD            CHAR(2) NOT NULL WITH DEFAULT,
           RPRT_RQST_ID        CHAR(4) NOT NULL,
           ACTVY_DT            DATE NOT NULL WITH DEFAULT,
           MGR_DIGIT_ONE       CHAR(1) NOT NULL WITH DEFAULT,
           MGR_LAST_NINE       CHAR(9) NOT NULL WITH DEFAULT,
           RPRT_LCTN           CHAR(6) NOT NULL WITH DEFAULT,
           RPRT_RQST_DESC      CHAR(35) NOT NULL WITH DEFAULT,
           COA_CD              CHAR(1) NOT NULL WITH DEFAULT,
           FROM_ACCT_INDX      CHAR(10) NOT NULL WITH DEFAULT,
           TO_ACCT_INDX        CHAR(10) NOT NULL WITH DEFAULT,
           FROM_FUND           CHAR(6) NOT NULL WITH DEFAULT,
           TO_FUND             CHAR(6) NOT NULL WITH DEFAULT,
           FROM_ORGN           CHAR(6) NOT NULL WITH DEFAULT,
           TO_ORGN             CHAR(6) NOT NULL WITH DEFAULT,
           FROM_SUB_ACCT       CHAR(6) NOT NULL WITH DEFAULT,
           TO_SUB_ACCT         CHAR(6) NOT NULL WITH DEFAULT,
           FROM_PRGRM          CHAR(6) NOT NULL WITH DEFAULT,
           TO_PRGRM            CHAR(6) NOT NULL WITH DEFAULT,
           FROM_ACTVY          CHAR(6) NOT NULL WITH DEFAULT,
           TO_ACTVY            CHAR(6) NOT NULL WITH DEFAULT,
           FROM_LCTN           CHAR(6) NOT NULL WITH DEFAULT,
           TO_LCTN             CHAR(6) NOT NULL WITH DEFAULT,
           EXCL_IND            CHAR(1) NOT NULL WITH DEFAULT,
           NBR_OF_COPIES       DECIMAL(3) NOT NULL WITH DEFAULT)
           END-EXEC.
       01  RID-RID-RQST-ID.
           05  RID-USER-CD                    PIC X(08).
           05  RID-LAST-ACTVY-DT              PIC X(05).
           05  RID-UNVRS-CD                   PIC X(02).
           05  RID-RPRT-RQST-ID               PIC X(04).
           05  RID-ACTVY-DT                   PIC X(10).
           05  RID-MGR-DIGIT-ONE              PIC X(01).
           05  RID-MGR-LAST-NINE              PIC X(09).
           05  RID-RPRT-LCTN                  PIC X(06).
           05  RID-RPRT-RQST-DESC             PIC X(35).
           05  RID-COA-CD                     PIC X(01).
           05  RID-FROM-ACCT-INDX             PIC X(10).
           05  RID-TO-ACCT-INDX               PIC X(10).
           05  RID-FROM-FUND                  PIC X(06).
           05  RID-TO-FUND                    PIC X(06).
           05  RID-FROM-ORGN                  PIC X(06).
           05  RID-TO-ORGN                    PIC X(06).
           05  RID-FROM-SUB-ACCT              PIC X(06).
           05  RID-TO-SUB-ACCT                PIC X(06).
           05  RID-FROM-PRGRM                 PIC X(06).
           05  RID-TO-PRGRM                   PIC X(06).
           05  RID-FROM-ACTVY                 PIC X(06).
           05  RID-TO-ACTVY                   PIC X(06).
           05  RID-FROM-LCTN                  PIC X(06).
           05  RID-TO-LCTN                    PIC X(06).
           05  RID-EXCL-IND                   PIC X(01).
           05  RID-NBR-OF-COPIES              PIC S9(03)         COMP-3.
