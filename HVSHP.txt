
           EXEC SQL DECLARE SHP_SHIP_TO_V TABLE
           (USER_CD            CHAR(8) NOT NULL WITH DEFAULT,
           LAST_ACTVY_DT       CHAR(5) NOT NULL WITH DEFAULT,
           UNVRS_CD            CHAR(2) NOT NULL WITH DEFAULT,
           SHIP_TO_CD          CHAR(6) NOT NULL,
           ADR_1               CHAR(35) NOT NULL WITH DEFAULT,
           ADR_2               CHAR(35) NOT NULL WITH DEFAULT,
           ADR_3               CHAR(35) NOT NULL WITH DEFAULT,
           ADR_4               CHAR(35) NOT NULL WITH DEFAULT,
           CITY_NAME           CHAR(18) NOT NULL WITH DEFAULT,
           STATE_CD            CHAR(2) NOT NULL WITH DEFAULT,
           ZIP_CD              CHAR(10) NOT NULL WITH DEFAULT,
           CNTCT_NAME          CHAR(35) NOT NULL WITH DEFAULT,
           TLPHN_ID            CHAR(14) NOT NULL WITH DEFAULT,
           ORGN_CD             CHAR(6) NOT NULL WITH DEFAULT,
           ROUTE_CD            CHAR(4) NOT NULL WITH DEFAULT,
           ACTVY_DT            DATE NOT NULL WITH DEFAULT,
           CNTRY_CD            CHAR(2) NOT NULL WITH DEFAULT,
           COA_CD              CHAR(1) NOT NULL WITH DEFAULT,
           SHIP_TYP_CD         CHAR(1) NOT NULL WITH DEFAULT,
           CNTCT_NM_UPPER      CHAR(35) NOT NULL WITH DEFAULT,
           START_DT            DATE NOT NULL WITH DEFAULT,
           END_DT              DATE NOT NULL WITH DEFAULT,
           IDX_NAME_UP_TS      TIMESTAMP NOT NULL WITH DEFAULT)
           END-EXEC.
       01  SHP-SHP-SHIP-TO.
           05  SHP-USER-CD                    PIC X(08).
           05  SHP-LAST-ACTVY-DT              PIC X(05).
           05  SHP-UNVRS-CD                   PIC X(02).
           05  SHP-SHIP-TO-CD                 PIC X(06).
           05  SHP-ADR-1                      PIC X(35).
           05  SHP-ADR-2                      PIC X(35).
           05  SHP-ADR-3                      PIC X(35).
           05  SHP-ADR-4                      PIC X(35).
           05  SHP-CITY-NAME                  PIC X(18).
           05  SHP-STATE-CD                   PIC X(02).
           05  SHP-ZIP-CD                     PIC X(10).
           05  SHP-CNTCT-NAME                 PIC X(35).
           05  SHP-TLPHN-ID                   PIC X(0014).
           05  SHP-ORGN-CD                    PIC X(06).
           05  SHP-ROUTE-CD                   PIC X(04).
           05  SHP-ACTVY-DT                   PIC X(10).
           05  SHP-CNTRY-CD                   PIC X(02).
           05  SHP-COA-CD                     PIC X(01).
           05  SHP-SHIP-TYP-CD                PIC X(01).
           05  SHP-CNTCT-NM-UPPER             PIC X(35).
           05  SHP-START-DT                   PIC X(10).
           05  SHP-END-DT                     PIC X(10).
           05  SHP-IDX-NAME-UP-TS             PIC X(26).
