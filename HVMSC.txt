
           EXEC SQL DECLARE MSC_MSGCMT_V TABLE
           (IDD_SEQ            DECIMAL(8) NOT NULL WITH DEFAULT,
           CMT_INFO_1          CHAR(50) NOT NULL WITH DEFAULT,
           CMT_INFO_2          CHAR(50) NOT NULL WITH DEFAULT,
           CMT_ID              DECIMAL(8) NOT NULL WITH DEFAULT,
           FK_MSG_MSG_KEY      CHAR(8) NOT NULL,
           MSG_TS              TIMESTAMP NOT NULL)
           END-EXEC.
       01  MSC-MSC-MSGCMT.
           05  MSC-IDD-SEQ                    PIC S9(8)          COMP-3.
           05  MSC-CMT-INFO-1                 PIC X(50).
           05  MSC-CMT-INFO-2                 PIC X(50).
           05  MSC-CMT-ID                     PIC S9(8)          COMP-3.
           05  MSC-FK-MSG-MSG-KEY             PIC X(08).
           05  MSC-MSG-TS                     PIC X(26).
