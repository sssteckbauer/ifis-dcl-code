
           EXEC SQL DECLARE CDP_CMD_PRED_V TABLE
           (USER_CD            CHAR(8) NOT NULL WITH DEFAULT,
           LAST_ACTVY_DT       CHAR(5) NOT NULL WITH DEFAULT,
           CDY_SETF            CHAR(1) NOT NULL WITH DEFAULT,
           FK1_CDY_CMDTY_CD    CHAR(8) NOT NULL WITH DEFAULT,
           CDY_SET_TS1         TIMESTAMP NOT NULL WITH DEFAULT,
           FK2_CDY_CMDTY_CD    CHAR(8) NOT NULL,
           CDY_SET_TS2         TIMESTAMP NOT NULL)
           END-EXEC.
       01  CDP-CDP-CMD-PRED.
           05  CDP-USER-CD                    PIC X(08).
           05  CDP-LAST-ACTVY-DT              PIC X(05).
           05  CDP-CDY-SETF                   PIC X(1).
           05  CDP-FK1-CDY-CMDTY-CD           PIC X(08).
           05  CDP-CDY-SET-TS1                PIC X(26).
           05  CDP-FK2-CDY-CMDTY-CD           PIC X(08).
           05  CDP-CDY-SET-TS2                PIC X(26).
