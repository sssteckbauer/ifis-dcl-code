
           EXEC SQL DECLARE IVT_INV_ACCT_V TABLE
           (USER_CD            CHAR(8) NOT NULL WITH DEFAULT,
           LAST_ACTVY_DT       CHAR(5) NOT NULL WITH DEFAULT,
           SEQ_NBR             DECIMAL(4) NOT NULL,
           TRVL_ACCT_DGT_ONE   CHAR(1) NOT NULL WITH DEFAULT,
           TRVL_ACCT_LST_NINE  CHAR(9) NOT NULL WITH DEFAULT,
           FK_EVT_EVENT_NBR    CHAR(8) NOT NULL WITH DEFAULT,
           BILL_TYP_IND        CHAR(1) NOT NULL WITH DEFAULT,
           ACCT_INVD_AMT       DECIMAL(8,2) NOT NULL WITH DEFAULT,
           BANK_ACCT_CD        CHAR(2) NOT NULL WITH DEFAULT,
           RULE_CLS_CD         CHAR(4) NOT NULL WITH DEFAULT,
           NSF_OVRDE           CHAR(1) NOT NULL WITH DEFAULT,
           COA_CD              CHAR(1) NOT NULL WITH DEFAULT,
           ACCT_INDX_CD        CHAR(10) NOT NULL WITH DEFAULT,
           FUND_CD             CHAR(6) NOT NULL WITH DEFAULT,
           ORGN_CD             CHAR(6) NOT NULL WITH DEFAULT,
           ACCT_CD             CHAR(6) NOT NULL WITH DEFAULT,
           PRGRM_CD            CHAR(6) NOT NULL WITH DEFAULT,
           ACTVY_CD            CHAR(6) NOT NULL WITH DEFAULT,
           LCTN_CD             CHAR(6) NOT NULL WITH DEFAULT,
           ACCT_ERROR_IND      CHAR(1) NOT NULL WITH DEFAULT,
           FSCL_YR             CHAR(2) NOT NULL WITH DEFAULT,
           PSTNG_PRD           CHAR(2) NOT NULL WITH DEFAULT,
           ACRL_IND            CHAR(1) NOT NULL WITH DEFAULT,
           PAID_IND            CHAR(1) NOT NULL WITH DEFAULT,
           FK_TRI_DOC_NBR      CHAR(8) NOT NULL,
           EVT_SETF            CHAR(1) NOT NULL WITH DEFAULT,
           EVT_SET_TS          TIMESTAMP NOT NULL WITH DEFAULT)
           END-EXEC.
       01  IVT-IVT-INV-ACCT.
           05  IVT-USER-CD                    PIC X(08).
           05  IVT-LAST-ACTVY-DT              PIC X(05).
           05  IVT-SEQ-NBR                    PIC S9(04)         COMP-3.
           05  IVT-TRVL-ACCT-DGT-ONE          PIC X(01).
           05  IVT-TRVL-ACCT-LST-NINE         PIC X(09).
           05  IVT-FK-EVT-EVENT-NBR           PIC X(08).
           05  IVT-BILL-TYP-IND               PIC X(01).
           05  IVT-ACCT-INVD-AMT              PIC S9(06)V99      COMP-3.
           05  IVT-BANK-ACCT-CD               PIC X(02).
           05  IVT-RULE-CLS-CD                PIC X(04).
           05  IVT-NSF-OVRDE                  PIC X(01).
           05  IVT-COA-CD                     PIC X(01).
           05  IVT-ACCT-INDX-CD               PIC X(10).
           05  IVT-FUND-CD                    PIC X(06).
           05  IVT-ORGN-CD                    PIC X(06).
           05  IVT-ACCT-CD                    PIC X(06).
           05  IVT-PRGRM-CD                   PIC X(06).
           05  IVT-ACTVY-CD                   PIC X(06).
           05  IVT-LCTN-CD                    PIC X(06).
           05  IVT-ACCT-ERROR-IND             PIC X(01).
           05  IVT-FSCL-YR                    PIC X(02).
           05  IVT-PSTNG-PRD                  PIC X(02).
           05  IVT-ACRL-IND                   PIC X(01).
           05  IVT-PAID-IND                   PIC X(01).
           05  IVT-FK-TRI-DOC-NBR             PIC X(08).
           05  IVT-EVT-SETF                   PIC X(1).
           05  IVT-EVT-SET-TS                 PIC X(26).
