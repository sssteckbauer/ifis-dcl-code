      ******************************************************************
      * DCLGEN TABLE(ACD_ACH_DATA_V)                                   *
      *        LIBRARY(FIST.FIS.DCLLIB(HVACD))                         *
      *        ACTION(REPLACE)                                         *
      *        LANGUAGE(COBOL)                                         *
      *        NAMES(ACD-)                                             *
      *        STRUCTURE(ACD-ACH-DATA-V)                               *
      *        QUOTE                                                   *
      *        COLSUFFIX(YES)                                          *
      * ... IS THE DCLGEN COMMAND THAT MADE THE FOLLOWING STATEMENTS   *
      ******************************************************************
           EXEC SQL DECLARE ACD_ACH_DATA_V TABLE
           ( USER_CD                        CHAR(8) NOT NULL,
             LAST_ACTVY_DT                  CHAR(5) NOT NULL,
             UNVRS_CD                       CHAR(2) NOT NULL,
             IREF_ID                        DECIMAL(7, 0) NOT NULL,
             ENTPSN_IND                     CHAR(1) NOT NULL,
             ACH_ID_DGT_ONE                 CHAR(1) NOT NULL,
             ACH_ID_LST_NINE                CHAR(9) NOT NULL,
             ACH_STOP_IND                   CHAR(1) NOT NULL,
             CTX_PYMT_IND                   CHAR(1) NOT NULL,
             IAT_PYMT_IND                   CHAR(1) NOT NULL
           ) END-EXEC.
      ******************************************************************
      * COBOL DECLARATION FOR TABLE ACD_ACH_DATA_V                     *
      ******************************************************************
       01  ACD-ACH-DATA-V.
           10 ACD-USER-CD          PIC X(8).
           10 ACD-LAST-ACTVY-DT    PIC X(5).
           10 ACD-UNVRS-CD         PIC X(2).
           10 ACD-IREF-ID          PIC S9(7)V USAGE COMP-3.
           10 ACD-ENTPSN-IND       PIC X(1).
           10 ACD-ACH-ID-DGT-ONE   PIC X(1).
           10 ACD-ACH-ID-LST-NINE  PIC X(9).
           10 ACD-ACH-STOP-IND     PIC X(1).
           10 ACD-CTX-PYMT-IND     PIC X(1).
           10 ACD-IAT-PYMT-IND     PIC X(1).
      ******************************************************************
      * THE NUMBER OF COLUMNS DESCRIBED BY THIS DECLARATION IS 9       *
      ******************************************************************
