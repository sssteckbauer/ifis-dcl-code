      ******************************************************************
      * DCLGEN TABLE(IVD_INV_DTL_V)                                    *
      *        ACTION(REPLACE)                                         *
      *        LANGUAGE(COBOL)                                         *
      *        NAMES(IVD-)                                             *
      *        STRUCTURE(IVD-IVD-INV-DTL)                              *
      *        QUOTE                                                   *
      *        COLSUFFIX(YES)                                          *
      *
      * ADD THREE NEW FIELDS - DMW 03/24/04
      *        TRD_IN_IND
      *        TRD_IN_AMT
      ******************************************************************
           EXEC SQL DECLARE IVD_INV_DTL_V TABLE
           ( USER_CD                        CHAR(8) NOT NULL,
             LAST_ACTVY_DT                  CHAR(5) NOT NULL,
             ITEM_NBR                       DECIMAL(4, 0) NOT NULL,
             CMDTY_CD                       CHAR(8) NOT NULL,
             OPN_CLS_HLD_IND                CHAR(1) NOT NULL,
             ACCPT_QTY                      DECIMAL(8, 2) NOT NULL,
             ACCPT_UNIT_PRC                 DECIMAL(14, 4) NOT NULL,
             PO_NBR                         CHAR(8) NOT NULL,
             CMDTY_DESC                     CHAR(35) NOT NULL,
             UNIT_MEA_CD                    CHAR(3) NOT NULL,
             DTL_CNTR_ACCT                  DECIMAL(4, 0) NOT NULL,
             DTL_ERROR_IND                  CHAR(1) NOT NULL,
             DSCNT_AMT                      DECIMAL(12, 2) NOT NULL,
             TAX_AMT                        DECIMAL(12, 2) NOT NULL,
             APRVD_QTY                      DECIMAL(8, 2) NOT NULL,
             APRVD_UNT_PRC                  DECIMAL(14, 4) NOT NULL,
             INVD_QTY                       DECIMAL(8, 2) NOT NULL,
             INVD_UNIT_PRC                  DECIMAL(14, 4) NOT NULL,
             PO_ITEM_NBR                    DECIMAL(4, 0) NOT NULL,
             TLRNC_OVRD_IND                 CHAR(1) NOT NULL,
             PREV_PAID_AMT                  DECIMAL(12, 2) NOT NULL,
             FK_IVH_DOC_NBR                 CHAR(8) NOT NULL,
             POD_SETF                       CHAR(1) NOT NULL,
             FK_POD_PO_NBR                  CHAR(8) NOT NULL,
             FK_POD_CHG_SEQ_NBR             CHAR(3) NOT NULL,
             FK_POD_ITEM_NBR                DECIMAL(4, 0) NOT NULL,
             POD_SET_TS                     TIMESTAMP NOT NULL,
             TRD_IN_IND                     CHAR(1) NOT NULL,
             TRD_IN_AMT                     DECIMAL(12, 2) NOT NULL,
             PROD_DSCNT_AMT                 DECIMAL(12, 2) NOT NULL
           ) END-EXEC.
      ******************************************************************
      * COBOL DECLARATION FOR TABLE         IVD_INV_DTL_V              *
      ******************************************************************
       01  IVD-IVD-INV-DTL.
      *                       USER_CD
           10 IVD-USER-CD          PIC X(8).
      *                       LAST_ACTVY_DT
           10 IVD-LAST-ACTVY-DT    PIC X(5).
      *                       ITEM_NBR
           10 IVD-ITEM-NBR         PIC S9(4)V USAGE COMP-3.
      *                       CMDTY_CD
           10 IVD-CMDTY-CD         PIC X(8).
      *                       OPN_CLS_HLD_IND
           10 IVD-OPN-CLS-HLD-IND  PIC X(1).
      *                       ACCPT_QTY
           10 IVD-ACCPT-QTY        PIC S9(6)V9(2) USAGE COMP-3.
      *                       ACCPT_UNIT_PRC
           10 IVD-ACCPT-UNIT-PRC   PIC S9(10)V9(4) USAGE COMP-3.
      *                       PO_NBR
           10 IVD-PO-NBR           PIC X(8).
      *                       CMDTY_DESC
           10 IVD-CMDTY-DESC       PIC X(35).
      *                       UNIT_MEA_CD
           10 IVD-UNIT-MEA-CD      PIC X(3).
      *                       DTL_CNTR_ACCT
           10 IVD-DTL-CNTR-ACCT    PIC S9(4)V USAGE COMP-3.
      *                       DTL_ERROR_IND
           10 IVD-DTL-ERROR-IND    PIC X(1).
      *                       DSCNT_AMT
           10 IVD-DSCNT-AMT        PIC S9(10)V9(2) USAGE COMP-3.
      *                       TAX_AMT
           10 IVD-TAX-AMT          PIC S9(10)V9(2) USAGE COMP-3.
      *                       APRVD_QTY
           10 IVD-APRVD-QTY        PIC S9(6)V9(2) USAGE COMP-3.
      *                       APRVD_UNT_PRC
           10 IVD-APRVD-UNT-PRC    PIC S9(10)V9(4) USAGE COMP-3.
      *                       INVD_QTY
           10 IVD-INVD-QTY         PIC S9(6)V9(2) USAGE COMP-3.
      *                       INVD_UNIT_PRC
           10 IVD-INVD-UNIT-PRC    PIC S9(10)V9(4) USAGE COMP-3.
      *                       PO_ITEM_NBR
           10 IVD-PO-ITEM-NBR      PIC S9(4)V USAGE COMP-3.
      *                       TLRNC_OVRD_IND
           10 IVD-TLRNC-OVRD-IND   PIC X(1).
      *                       PREV_PAID_AMT
           10 IVD-PREV-PAID-AMT    PIC S9(10)V9(2) USAGE COMP-3.
      *                       FK_IVH_DOC_NBR
           10 IVD-FK-IVH-DOC-NBR   PIC X(8).
      *                       POD_SETF
           10 IVD-POD-SETF         PIC X(1).
      *                       FK_POD_PO_NBR
           10 IVD-FK-POD-PO-NBR    PIC X(8).
      *                       FK_POD_CHG_SEQ_NBR
           10 IVD-FK-POD-CHG-SEQ-NBR  PIC X(3).
      *                       FK_POD_ITEM_NBR
           10 IVD-FK-POD-ITEM-NBR  PIC S9(4)V USAGE COMP-3.
      *                       POD_SET_TS
           10 IVD-POD-SET-TS       PIC X(26).
      *                       TRD_IN_IND
           10 IVD-TRD-IN-IND       PIC X(1).
      *                       TRD_IN_AMT
           10 IVD-TRD-IN-AMT       PIC S9(10)V9(2) USAGE COMP-3.
      *                       PROD_DSCNT_AMT
           10 IVD-PROD-DSCNT-AMT   PIC S9(10)V9(2) USAGE COMP-3.
      ******************************************************************
      * THE NUMBER OF COLUMNS DESCRIBED BY THIS DECLARATION IS 30      *
      ******************************************************************
